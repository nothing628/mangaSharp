﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangaSharp.Info
{
    public interface IChapter
    {
        string Title { get; set; }
        int ChapterOrder { get; set; }
        Uri ChapterUrl { get; set; }
        Task<List<IPage>> GetPages();
        Task<bool> DownloadChapter(string LocalPath);
    }
    public abstract class Chapter : IChapter
    {
        public abstract string Title { get; set; }
        public abstract int ChapterOrder { get; set; }
        public abstract Uri ChapterUrl { get; set; }
        public abstract Task<List<IPage>> GetPages();
        public abstract Task<bool> DownloadChapter(string LocalPath);
    }
}
