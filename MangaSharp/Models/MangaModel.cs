﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MangaSharp.Models
{
    public class MangaModel : INotifyPropertyChanged
    {
        public string Title { get; set; }
        public string Artist { get; set; }
        public int ChapterCount { get; set; }
        public int PageCount { get; set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
        
    }
}
