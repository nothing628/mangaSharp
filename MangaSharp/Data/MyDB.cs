﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
using System.IO;

namespace MangaSharp.Data
{
    public class MyDB
    {
        private static LiteDatabase myDatabase;

        static MyDB()
        {
            ConnectionString str = new ConnectionString();

            str.LimitSize = 2147483000L;
            str.Mode = LiteDB.FileMode.Exclusive;
            str.InitialSize = 1024;
            str.Journal = true;
            str.Filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "lite.db");

            myDatabase = new LiteDatabase(str);
        }
        
        public MyDB()
        {
            
        }

        public static LiteDatabase GetLiteDatabase()
        {
            return myDatabase;
        }
    }
}
