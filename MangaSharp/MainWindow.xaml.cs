﻿using MangaSharp.Layout;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MangaSharp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private bool _IsExpand;
        public bool IsExpand {
            get
            {
                return _IsExpand;
            }
            set {
                if (_IsExpand != value)
                {
                    _IsExpand = value;
                    FirePropertyChanged("IsExpand");
                }
            }
        }

        private void FirePropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindow()
        {
            IsExpand = false;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mainFrame.NavigationService.Navigate(new Home());
        }

        private void MenuClick(object sender, MouseButtonEventArgs e)
        {
            if (((StackPanel) sender).Equals(ExpandMenu))
                IsExpand = !IsExpand;
            if (((StackPanel)sender).Equals(SearchMenu))
                mainFrame.NavigationService.Navigate(new Search());
        }
    }
}
