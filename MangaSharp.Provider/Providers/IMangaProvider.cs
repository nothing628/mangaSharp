﻿using MangaSharp.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangaSharp.Providers
{
    public interface IMangaProvider
    {
        Task<List<IManga>> FetchManga(int Page = 1, string Keyword = "");
        bool IsSupportSearch { get; }
    }

    public abstract class MangaProvider : IMangaProvider
    {
        public abstract bool IsSupportSearch { get; }
        public abstract Task<List<IManga>> FetchManga(int Page = 1, string Keyword = "");
    }
}
