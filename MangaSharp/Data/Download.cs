﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangaSharp.Data
{
    public class TManga
    {
        // Id by GUID
        [BsonId]
        public Guid Id { get; set; }

        // Title manga
        public string Title { get; set; }

        // Thumbnail in online version
        public string UrlThumb { get; set; }

        // Where website url for this manga
        public string UrlManga { get; set; }

        // Where to offline folder save
        public string PathManga { get; set; }

        // Store pages information
        public List<TPage> Pages { get; set; } = new List<TPage>();
        
        // Is already download all pages
        [BsonIgnore]
        public bool IsDownloaded
        {
            get
            {
                return Pages.Where(x => x.IsDownloaded).ToList().Count == Pages.Count;
            }
        }
    }

    public class TPage
    {
        // Page number
        public int PageOrder { get; set; }

        // Online Image
        public string UrlPage { get; set; }

        // Offline Image
        public string PathPage { get; set; }

        //Is already downloaded
        public bool IsDownloaded { get; set; }
    }
}
