﻿using MangaSharp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MangaSharp.Layout
{
    /// <summary>
    /// Interaction logic for Search.xaml
    /// </summary>
    public partial class Search : Page
    {
        private ObservableCollection<MangaModel> listManga = new ObservableCollection<MangaModel>();
        public ObservableCollection<MangaModel> ListManga
        {
            get
            {
                return listManga;
            }
            set
            {
                listManga = value;
            }
        }

        public Search()
        {
            InitializeComponent();
            ListManga.Add(new MangaModel()
            {
                Title = "Test Search",
                ChapterCount = 122,
                PageCount = 22,
            });

            ListManga.Add(new MangaModel()
            {
                Title = "Test 2 Search",
                ChapterCount = 12,
                PageCount = 222,
            });
        }

        private void SearchKeyword(object sender, RoutedEventArgs e)
        {
            mSearchGrid.CanUserDeleteRows = true;
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
        }
    }
}
