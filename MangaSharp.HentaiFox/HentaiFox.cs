﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MangaSharp.Info;
using System.Net.Http;
using DManga = MangaSharp.Info.Manga;
using DChapter = MangaSharp.Info.Chapter;
using DPage = MangaSharp.Info.Page;
using AngleSharp;
using AngleSharp.Parser.Html;
using AngleSharp.Dom.Html;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;

namespace MangaSharp.Providers.HentaiFox
{
    public class Provider : MangaProvider
    {
        private string baseUri = "https://hentaifox.com/pag/{0}/";

        public override bool IsSupportSearch
        {
            get
            {
                return false;
            }
        }
        
        public override async Task<List<IManga>> FetchManga(int Page = 1, string Keyword = "")
        {
            List<IManga> result = new List<IManga>();
            HtmlParser parser = new HtmlParser();
            Uri homeUri = new Uri(String.Format(baseUri, Page));

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(homeUri, HttpCompletionOption.ResponseHeadersRead);
                    HttpContent content = response.Content;
                    var i = await parser.ParseAsync(await content.ReadAsStreamAsync());
                    var links = i.QuerySelectorAll(".block.overview > .item > a");

                    foreach (var item in links)
                    {
                        string Title = item.QuerySelector(".caption").TextContent;
                        string Image = item.QuerySelector(".image > img").GetAttribute("src");
                        string Url = item.GetAttribute("href");
                        
                        if (!Image.StartsWith("https"))
                            Image = "https:" + Image;

                        if (!Url.StartsWith("https"))
                            Url = "https://" + homeUri.Host + Url;

                        result.Add(new Manga() { Title = CleanText(Title), MangaUrl = new Uri(Url), ThumbUrl = new Uri(Image) });
                    }
                } catch
                {
                    //
                }
            }

            return result;
        }

        public string CleanText(string Text)
        {
            return Text.Replace("\t", string.Empty).Replace("\n", string.Empty);
        }
        
    }
    public class Manga : DManga
    {
        public override string Title { get; set; }
        public override int PagesCount { get; }
        public override Uri ThumbUrl { get; set; }
        public override Uri MangaUrl { get; set; }

        public override async Task<bool> DownloadManga(string LocalPath)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            string destination = Path.Combine(LocalPath, r.Replace(Title, ""));
            bool is_success = true;

            if (!Directory.Exists(destination))
                Directory.CreateDirectory(destination);

            foreach (Chapter chapter in GetChapters())
            {
                var res = await chapter.DownloadChapter(destination);

                if (!res)
                    is_success = false;
            }

            return is_success;
        }
        
        public override List<IChapter> GetChapters()
        {
            List<IChapter> result = new List<IChapter>();

            //HentaiFox doesn't have chapter
            result.Add(new Chapter() {
                Title = Title,
                ChapterOrder = 1,
                ChapterUrl = MangaUrl
            });

            return result;
        }

        public async override Task<List<IPage>> GetPages()
        {
            List<IPage> pages = new List<IPage>();

            foreach (Chapter chapter in GetChapters())
            {
                pages.AddRange(await chapter.GetPages());
            }

            return pages;
        }
    }
    public class Chapter : DChapter
    {
        public override string Title { get; set; }
        public override int ChapterOrder { get; set; }
        public override Uri ChapterUrl { get; set; }

        public override async Task<bool> DownloadChapter(string LocalPath)
        {
            var is_success = true;
            var pageCounter = 1;

            foreach (Page page in await GetPages())
            {
                var pageDestination = Path.Combine(LocalPath, pageCounter.ToString("000"));
                var res = await page.DownloadPage(pageDestination);

                if (!res)
                    is_success = false;

                pageCounter++;
            }

            return is_success;
        }
        
        public override async Task<List<IPage>> GetPages()
        {
            List<IPage> list = new List<IPage>();
            HtmlParser parser = new HtmlParser();
            string baseUri = "https://" + ChapterUrl.Host;

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(ChapterUrl, HttpCompletionOption.ResponseHeadersRead);
                HttpContent content = response.Content;
                var htmlDoc = await parser.ParseAsync(await content.ReadAsStreamAsync());
                var pages = htmlDoc.QuerySelectorAll(".preview_thumb > a");

                foreach (var page in pages)
                {
                    string link = page.GetAttribute("href");

                    list.Add(new Page()
                    {
                        PageUrl = new Uri(baseUri + link),
                        LocalPath = ""
                    });
                }
            }

            return list;
        }
    }
    public class Page : DPage
    {
        public override int Width { get; }
        public override int Height { get; }
        public override string LocalPath { get; set; }
        public override Uri PageUrl { get; set; }
        public override Dimension Dimension { get; }

        public override async Task<bool> DownloadPage(string LocalPath)
        {
            var is_success = false;
            HtmlParser parser = new HtmlParser();

            using (WebClient webClient = new WebClient())
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(PageUrl, HttpCompletionOption.ResponseHeadersRead);
                    HttpContent content = response.Content;
                    var ihtml = await parser.ParseAsync(await content.ReadAsStreamAsync());
                    var image = ihtml.GetElementById("gimg");
                    var imageSrc = image.GetAttribute("src");
                    var ext = GetExt(imageSrc);

                    if (!imageSrc.StartsWith("https:"))
                        imageSrc = "https:" + imageSrc;

                    webClient.DownloadProgressChanged += WebClient_DownloadProgressChanged;
                    webClient.DownloadFileCompleted += WebClient_DownloadFileCompleted;

                    await webClient.DownloadFileTaskAsync(new Uri(imageSrc), LocalPath + "." + ext);

                    webClient.DownloadProgressChanged -= WebClient_DownloadProgressChanged;
                    webClient.DownloadFileCompleted -= WebClient_DownloadFileCompleted;
                } catch (Exception ex)
                {
                    is_success = false;
                }
            }

            return is_success;
        }

        private void WebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Console.WriteLine("Downloading : " + e.ProgressPercentage + "%");
        }

        private void WebClient_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            Console.WriteLine("Download Complete");
        }

        private string GetExt(string contentType)
        {
            return contentType.Substring(contentType.Length - 3, 3);
            switch (contentType)
            {
                case "image/jpeg":
                case "image/jpg":
                    return "jpg";
                    break;
                default:
                    return "";
            }
        }
    }
}
