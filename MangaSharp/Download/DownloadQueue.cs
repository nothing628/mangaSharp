﻿using MangaSharp.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangaSharp.Download
{
    public class DownloadQueue
    {
        public delegate void DownloadProgressHandler(int progress, int totalDownload, int totalItem);
        public event DownloadProgressHandler DownloadProgressEvent;

        public string DownloadTitle { get; set; }
        public int DownloadProgress { get; }
        public string DownloadPath { get; set; }

        private List<IPage> DownloadItem;

        public DownloadQueue()
        {
            DownloadItem = new List<IPage>();
        }

        public DownloadQueue(List<IPage> DownloadItems)
        {
            DownloadItem = new List<IPage>();
            DownloadItem.AddRange(DownloadItems);
        }
    }
}
