﻿using MangaSharp.Download;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MangaSharp.Layout
{
    /// <summary>
    /// Interaction logic for Download.xaml
    /// </summary>
    public partial class Download : Page
    {
        public Download()
        {
            InitializeComponent();
            DownloadManager.DownloadProgressEvent += DownloadManager_DownloadProgressEvent;
        }

        private void DownloadManager_DownloadProgressEvent(int progress, int totalDownload, int totalItem)
        {
            throw new NotImplementedException();
        }

        public DownloadQueue DownloadManager;
    }
}
