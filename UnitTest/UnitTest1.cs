﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using AngleSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        private Uri homeUri = new Uri("https://hentaifox.com/pag/1/");

        [TestMethod]
        public void TestZipLib()
        {
            FastZipCreate(@"E:\tosidenn\");
        }

        private int _uptoFileCount;
        private int _totalFileCount;

        public void FastZipCreate(string backupFolderPath)
        {

            _totalFileCount = FolderContentsCount(backupFolderPath);

            FastZipEvents events = new FastZipEvents();
            events.ProcessFile = ProcessFileMethod;
            FastZip fastZip = new FastZip(events);

            fastZip.CreateEmptyDirectories = true;
            fastZip.Password = "123456";
            string zipFileName = Directory.GetParent(backupFolderPath).FullName + "\\ZipTest.zip";

            fastZip.CreateZip(zipFileName, backupFolderPath, true, "");
        }

        private void ProcessFileMethod(object sender, ScanEventArgs args)
        {
            _uptoFileCount++;
            int percentCompleted = _uptoFileCount * 100 / _totalFileCount;
            // do something here with a progress bar
            // file counts are easier as sizes take more work to calculate, and compression levels vary by file type

            string fileName = args.Name;
            // To terminate the process, set args.ContinueRunning = false
            if (fileName == "stop on this file")
                args.ContinueRunning = false;
        }

        // Returns the number of files in this and all subdirectories
        private int FolderContentsCount(string path)
        {
            int result = Directory.GetFiles(path).Length;
            string[] subFolders = Directory.GetDirectories(path);
            foreach (string subFolder in subFolders)
            {
                result += FolderContentsCount(subFolder);
            }
            return result;
        }

        [TestMethod]
        public void TestMethod1()
        {
            var x = Test();
            x.Wait();
        }

        public async Task Test()
        {
            List<Manga> result = new List<Manga>();

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(homeUri, HttpCompletionOption.ResponseHeadersRead);
                    HttpContent content = response.Content;
                    
                } catch (Exception ex)
                {
                    var x = ex.ToString();
                }
            }
        }

        public string CleanText(string Text)
        {
            return Text.Replace("\t", string.Empty).Replace("\n", string.Empty);
        }
    }

    public class Manga
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string ImgThumb { get; set; }
    }
}
