﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MangaSharp.Info
{
    public interface IManga
    {
        string Title { get; set; }
        int PagesCount { get; }
        Uri ThumbUrl { get; set; }
        Uri MangaUrl { get; set; }
        List<IChapter> GetChapters();
        Task<List<IPage>> GetPages();
        Task<bool> DownloadManga(string LocalPath);
    }
    public abstract class Manga : IManga
    {
        public abstract string Title { get; set; }
        public abstract int PagesCount { get; }
        public abstract Uri ThumbUrl { get; set; }
        public abstract Uri MangaUrl { get; set; }
        public abstract List<IChapter> GetChapters();
        public abstract Task<List<IPage>> GetPages();
        public abstract Task<bool> DownloadManga(string LocalPath);
    }
}
