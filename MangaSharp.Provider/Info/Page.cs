﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangaSharp.Info
{
    public interface IPage
    {
        int Width { get; }
        int Height { get; }
        string LocalPath { get; set; }
        Uri PageUrl { get; set; }
        Dimension Dimension { get; }
        Task<bool> DownloadPage(string LocalPath);
    }

    public abstract class Page : IPage
    {
        public abstract int Width { get; }
        public abstract int Height { get; }
        public abstract string LocalPath { get; set; }
        public abstract Uri PageUrl { get; set; }
        public abstract Dimension Dimension { get; }
        public abstract Task<bool> DownloadPage(string LocalPath);
    }

    public struct Dimension
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsEmpty
        {
            get
            {
                return (Width <= 0 || Height <= 0);
            }
        }
    }
}
